@extends('layouts.admin_layout')
@push('css')
  <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Prestasi
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin/index"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="/admin/sertifikat">Prestasi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Prestasi</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Bidang</th>
                    <th>Nama</th> 
                    <th>Juara</th>
                    <th>Tingkat</th>
                    <th>Penyelenggara</th>
                    <th>Tahun</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($data as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->bidang}}</td>
                    <td>{{$d->nama}}</td>
                    <td>{{$d->kejuaraan}}</td>
                    <td>{{$d->tingkat}}</td>
                    <td>{{$d->penyelenggara}}</td>
                    <td>{{$d->tahun}}</td>
                    <td>{{$d->status}}</td>
                    <td>
                      <a href="#" class="edit btn btn-xs btn-primary" data-id='{{$d->id}}' data-nama='{{$d->nama}}' data-bidang='{{$d->bidang}}' data-kejuaraan='{{$d->kejuaraan}}' data-tingkat='{{$d->tingkat}}' data-penyelenggara='{{$d->penyelenggara}}' data-tahun='{{$d->tahun}}' data-waktu='{{$d->waktu}}' data-deskripsi='{{$d->deskripsi}}' data-image-sertifikat='{{$d->bukti_sertifikat}}' data-image-kegiatan='{{$d->bukti_kegiatan}}' data-status='{{$d->status}}' data-catatan='{{$d->catatan}}'><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/admin/prestasi" method="post" enctype="multipart/form-data" id="edit-form">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Prestasi</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Perlombaan" name="nama" id="detailNama">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Deskripsi" name="deskripsi" id="detailDeskripsi">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select name="kejuaraan" class="form-control" id="detailKejuaraan">
                    <option>Pilih Kejuaraan</option>
                    <option value="Juara 1">Juara 1</option>
                    <option value="Juara 2">Juara 2</option>
                    <option value="Juara 3">Juara 3</option>
                    <option value="Juara Harapan 1">Juara Harapan 1</option>
                    <option value="Juara Harapan 2">Juara Harapan 2</option>
                    <option value="Juara Harapan 3">Juara Harapan 3</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Penyelenggara" name="penyelenggara" id="detailPenyelenggara">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select name="tingkat" class="form-control" id="detailTingkat">
                    <option>Pilih Tingkat</option>
                    <option value="Lokal">Lokal</option>
                    <option value="Nasional">Nasional</option>
                    <option value="Regional">Regional</option>
                    <option value="Internasional">Internasional</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select name="bidang" class="form-control" id="detailBidang">
                    <option>Pilih Bidang</option>
                    <option value="Penalaran">Penalaran</option>
                    <option value="Seni">Seni</option>
                    <option value="Olahraga">Olahraga</option>
                    <option value="Minat Khusus">Minat Khusus</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="date" class="form-control" placeholder="Waktu" name="waktu" id="detailWaktu">
                </div>
              </div>

              <div class="col-md-12">
                <label class="form-control-label">Bukti Sertifikat</label>
                <div style="position: relative;">
                  <img src="" id="detailImageSertifikat" class="img-responsive" style="margin-left: auto; margin-right: auto;" alt="Gambar">
                </div>
                <br>
              </div>
              <div class="col-md-12">
                <label class="form-control-label">Bukti Kegiatan</label>
                <div style="position: relative;">
                  <img src="" id="detailImageKegiatan" class="img-responsive" style="margin-left: auto; margin-right: auto;" alt="Gambar">
                </div>
                <br>
              </div>
              <hr>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <select name="status" class="form-control" id="detailStatus">
                    <option value="Tahap Konfirmasi">Tahap Konfirmasi</option>
                    <option value="Revisi">Revisi</option>
                    <option value="Terverifikasi">Terverifikasi</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <textarea class="form-control" name="catatan" id="detailCatatan"></textarea>
                </div>
              </div>
              
              
            </div>
          </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailId">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  @endsection
  @push('js')
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('.dataTable').dataTable();

      });

      $('.edit').click(function(){
        var nama = $(this).data('nama');
        var deskripsi = $(this).data('deskripsi');
        var kejuaraan = $(this).data('kejuaraan');
        var penyelenggara = $(this).data('penyelenggara');
        var bidang = $(this).data('bidang');
        var tingkat = $(this).data('tingkat');
        var waktu = $(this).data('waktu');
        var status = $(this).data('status');
        var catatan = $(this).data('catatan');
        var imageSertifikat = $(this).data('image-sertifikat');
        var imageKegiatan = $(this).data('image-kegiatan');

        var id = $(this).data('id');
        $("#edit-form").attr("action", "/admin/prestasi/" + id);
        $('#detailDeskripsi').val(deskripsi);
        $('#detailNama').val(nama);
        $('#detailKejuaraan').val(kejuaraan);
        $('#detailPenyelenggara').val(penyelenggara);
        $('#detailTingkat').val(tingkat);
        $('#detailBidang').val(bidang);
        $('#detailWaktu').val(waktu);
        $('#detailStatus').val(status);
        $('#detailCatatan').html(catatan);
        $('#detailImageSertifikat').attr('src', '/images/prestasi/sertifikat/'+ imageSertifikat);
        $('#detailImageKegiatan').attr('src', '/images/prestasi/kegiatan/'+ imageKegiatan);
        $('#detailId').val(id);
        $('#detailModal').modal();

      });
      $('.delete').click(function() {

        var id = $(this).data('id');

        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Apakaha anda ingin menghapus data ini</b>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/admin/prestasi/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/admin/prestasi/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });
    </script>
  @endpush


