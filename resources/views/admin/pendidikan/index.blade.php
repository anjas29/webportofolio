@extends('layouts.admin_layout')
@push('css')
  <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Riwayat Pendidikan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin/index"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="/admin/pendidikan">Riwayat Pendidikan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Riwayat Pendidikan</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Jenjang</th> 
                    <th>Instansi</th>
                    <th>Periode</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($data as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->jenjang}}</td>
                    <td>{{$d->instansi}}</td>
                    <td>{{$d->tahun_masuk." - ".$d->tahun_kelulusan}}</td>
                    <td>{{$d->status}}</td>
                    <td>
                      <a href="#" class="edit btn btn-xs btn-primary" data-id='{{$d->id}}' data-jenjang='{{$d->jenjang}}' data-instansi='{{$d->instansi}}' data-tahun_masuk='{{$d->tahun_masuk}}' data-tahun_lulus='{{$d->tahun_lulus}}' data-image='{{$d->bukti_ijazah}}' data-status='{{$d->status}}' data-catatan='{{$d->catatan}}'><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- crate Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='createModal'>
    <div class="modal-dialog" role="document">
      <form action="/admin/pendidikan" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Tambah Riwayat Pendidikan</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select class="form-control" name="jenjang">
                    <option>Pilih Jenjang</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="Diploma">Diploma</option>
                    <option value="Sarjana">Sarjana</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Instansi" name="instansi">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun Masuk" name="tahun_masuk">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun Kelulusan" name="tahun_lulus">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="file" name="bukti_ijazah" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/admin/pendidikan" method="post" enctype="multipart/form-data" id="edit-form">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Riwayat Pendidikan</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select class="form-control" name="jenjang" id="detailJenjang">
                    <option>Pilih Jenjang</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="Diploma">Diploma</option>
                    <option value="Sarjana">Sarjana</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Instansi" name="instansi" id="detailInstasi">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun Masuk" name="tahun_masuk" id="detailTahunMasuk">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun Kelulusan" name="tahun_lulus" id="detailTahunKelulusan">
                </div>
              </div>
              <div class="col-md-12">
                <div style="position: relative;">
                  <img src="" id="detailImage" class="img-responsive" style="margin-left: auto; margin-right: auto;" alt="Gambar">
                </div>
                <br>
              </div>
              <hr>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <select name="status" class="form-control" id="detailStatus">
                    <option value="Tahap Konfirmasi">Tahap Konfirmasi</option>
                    <option value="Ditolak">Pengajuan ditolak</option>
                    <option value="Diterima">Terverifikasi</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <textarea class="form-control" name="catatan" id="detailCatatan"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailId">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  @endsection
  @push('js')
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });

      $('.edit').click(function(){
        var jenjang = $(this).data('jenjang');
        var instansi = $(this).data('instansi');
        var tahun_masuk = $(this).data('tahun_masuk');
        var tahun_lulus = $(this).data('tahun_lulus');     
        var image = $(this).data('image');
        var status = $(this).data('status');
        var catatan = $(this).data('catatan');

        var id = $(this).data('id');
        $("#edit-form").attr("action", "/admin/pendidikan/" + id);

        $('#detailJenjang').val(jenjang);
        $('#detailInstasi').val(instansi);
        $('#detailTahunMasuk').val(tahun_masuk);
        $('#detailTahunKelulusan').val(tahun_lulus);
        $('#detailStatus').val(status);
        $('#detailCatatan').html(catatan);
        $('#detailImage').attr('src', '/images/pendidikan/'+ image);
        $('#detailId').val(id);
        $('#detailModal').modal();

      });
      $('.delete').click(function() {

        var id = $(this).data('id');

        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Apakaha anda ingin menghapus data ini</b>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/admin/pendidikan/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/admin/pendidikan/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });
    </script>
  @endpush

