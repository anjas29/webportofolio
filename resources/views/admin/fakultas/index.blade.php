@extends('layouts.admin_layout')
@push('css')
  <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Fakultas
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin/index"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active"><a href="/administrator/admin">Fakultas</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Fakultas</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <p><button class="btn btn-sm btn-primary pull-right" data-toggle='modal' data-target="#createModal">Tambah Fakultas</button></p>
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Fakultas</th>
                    <th>Aksi</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($fakultas as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->nama}}</td>
                    <td>
                      <a href="#" class="edit btn btn-xs btn-primary" data-id='{{$d->id}}' data-nama='{{$d->nama}}' ><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}' data-nama='{{$d->nama}}' ><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Prodi</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <p><button class="btn btn-sm btn-primary pull-right" data-toggle='modal' data-target="#createModalProdi">Tambah Prodi</button><br></p>
              <hr>
              <table class="table table-striped table-bordered dataTableProdi">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Fakultas</th>
                    <th>Nama Prodi</th>
                    <th>Aksi</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($prodi as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->fakultas->nama}}</td>
                    <td>{{$d->nama}}</td>
                    <td>
                      <a href="#" class="editProdi btn btn-xs btn-primary" data-id='{{$d->id}}' data-nama='{{$d->nama}}' data-fakultas_id='{{$d->fakultas_id}}' ><i class="fa fa-pencil"></i></a>
                      <a href="#" class="deleteProdi btn btn-xs btn-danger" data-id='{{$d->id}}' data-nama='{{$d->nama}}' data-fakultas='{{$d->fakultas->nama}}' ><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- crate Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='createModal'>
    <div class="modal-dialog" role="document">
      <form action="/admin/fakultas" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Tambah Fakultas</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Fakultas" name="nama">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!-- crate Class Modal Prodi -->
  <div class="modal fade" tabindex="-1" role="dialog" id='createModalProdi'>
    <div class="modal-dialog" role="document">
      <form action="/admin/prodi" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Tambah Prodi</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Prodi" name="nama">
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select name="fakultas_id" class="form-control">
                    <option >-- Pilih Fakultass --</option>
                    @foreach($fakultas as $f)
                      <option value="{{$f->id}}">{{$f->nama}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/admin/fakultas" method="post" id="edit-form-fakultas">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Fakultas</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Fakultas" id='detailNamaFakultas' name="nama">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailId">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModalProdi'>
    <div class="modal-dialog" role="document">
      <form action="/admin/prodi" method="post" id="edit-form-prodi">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Prodi</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Prodi" name="nama" id="detailNamaProdi">
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <select name="fakultas_id" id="detailFakultasId" class="form-control">
                    <option >-- Pilih Fakultas --</option>
                    @foreach($fakultas as $f)
                      <option value="{{$f->id}}">{{$f->nama}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailIdProdi">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  @endsection
  @push('js')
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('.dataTable').dataTable({
          "lengthChange": false,
          "searching": false,
          "paging":   false,
          "info":     false
        });
        $('.dataTableProdi').dataTable();
      });

      $('.edit').click(function(){
        var nama = $(this).data('nama');
        var id = $(this).data('id');
        $("#edit-form-fakultas").attr("action", "/admin/fakultas/" + id);

        $('#detailNamaFakultas').val(nama);
        $('#detailId').val(id);
        $('#detailModal').modal();

      });

      $('.editProdi').click(function(){
        var nama = $(this).data('nama');
        var fakultas_id = $(this).data('fakultas_id');
        var id = $(this).data('id');
        $("#edit-form-prodi").attr("action", "/admin/prodi/" + id);

        $('#detailNamaProdi').val(nama);
        $('#detailFakultasId').val(fakultas_id);
        $('#detailIdProdi').val(id);
        $('#detailModalProdi').modal();

      });

      $('.delete').click(function() {
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Hapus Fakultas</b> : <strong>"+nama+" </strong> (Tindakan ini sangat beresiko)?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Proses...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/admin/fakultas/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/admin/fakultas/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });

      $('.deleteProdi').click(function() {
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        var fakultas = $(this).data('fakultas');
        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Prodi : <strong>"+nama+",  "+fakultas+" </strong>?<br><strong>(Tindakan ini sangat beresiko)</strong>", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Proses...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/admin/prodi/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/admin/prodi/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });
    </script>
  @endpush