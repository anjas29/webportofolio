@extends('layouts.admin_layout')
@push('css')
  <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pekerjaan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin/index"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="/admin/pekerjaan">Pekerjaan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Pekerjaan</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Instasi</th> 
                    <th>Jabatan</th>
                    <th>Periode</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($data as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->instansi}}</td>
                    <td>{{$d->jabatan}}</td>
                    <td>{{$d->periode}}</td>
                    <td>{{$d->status}}</td>
                    <td>
                      <a href="#" class="edit btn btn-xs btn-primary" data-id='{{$d->id}}' data-nama='{{$d->instansi}}' data-jabatan='{{$d->jabatan}}' data-periode='{{$d->periode}}' data-image='{{$d->bukti_pengalaman_kerja}}' data-deskripsi='{{$d->deskripsi}}' data-status='{{$d->status}}' data-catatan='{{$d->catatan}}'><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

   <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/admin/pekerjaan" method="post" enctype="multipart/form-data" id="edit-form">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Pengalaman Pekerjaan</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Instansi" name="instansi" id="detailNama">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Jabatan" name="jabatan" id="detailJabatan">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <textarea name="deskripsi" class="form-control" placeholder="Deskripsi" id="detailDeskripsi"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Periode" name="periode" id="detailPeriode">
                </div>
              </div>
              <div class="col-md-12">
                <div style="position: relative;">
                  <img src="" id="detailImage" class="img-responsive" style="margin-left: auto; margin-right: auto;" alt="Gambar">
                </div>
                <br>
              </div>
              <hr>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <select name="status" class="form-control" id="detailStatus">
                    <option value="Tahap Konfirmasi">Tahap Konfirmasi</option>
                    <option value="Revisi">Revisi</option>
                    <option value="Terverifikasi">Terverifikasi</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                  <textarea class="form-control" name="catatan" id="detailCatatan"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailId">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  @endsection
  @push('js')
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });

      $('.edit').click(function(){
        var nama = $(this).data('nama');
        var jabatan = $(this).data('jabatan');
        var periode = $(this).data('periode');
        var deskripsi = $(this).data('deskripsi');
        var status = $(this).data('status');
        var catatan = $(this).data('catatan');
        var image = $(this).data('image');

        var id = $(this).data('id');
        $("#edit-form").attr("action", "/admin/pekerjaan/" + id);

        $('#detailNama').val(nama);
        $('#detailJabatan').val(jabatan);
        $('#detailPeriode').val(periode);
        $('#detailDeskripsi').html(deskripsi);
        $('#detailStatus').val(status);
        $('#detailCatatan').html(catatan);
        $('#detailImage').attr('src', '/images/pekerjaan/'+ image);
        $('#detailId').val(id);
        $('#detailModal').modal();

      });
      $('.delete').click(function() {

        var id = $(this).data('id');

        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Apakaha anda ingin menghapus data ini</b>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/admin/pekerjaan/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/admin/pekerjaan/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });
    </script>
  @endpush

