<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

 <title>Portofolio</title>

 <style type="text/css">
    * { margin: 0; padding: 0; }
    body { font: 16px Helvetica, Sans-Serif; line-height: 24px; background: url(/images/noise.jpg); }
    .clear { clear: both; }
    #page-wrap { width: 800px; margin: 40px auto 60px; }
    #pic { float: right; margin: 10px 0 0 0; }
    h1 { margin: 0 0 16px 0; padding: 0 0 16px 0; font-size: 42px; font-weight: bold; letter-spacing: -2px;  }
    h2 { font-size: 20px; margin: 0 0 6px 0; position: relative; }
    h2 span { position: absolute; bottom: 0; right: 0; font-style: italic; font-family: Georgia, Serif; font-size: 16px; color: #999; font-weight: normal; }
    .desc { bottom: 0; right: 0; font-style: italic; font-family: Georgia, Serif; font-size: 16px; color: #999; font-weight: normal; }
    p { margin: 0 0 16px 0; }
    a { color: #999; text-decoration: none; border-bottom: 1px dotted #999; }
    a:hover { border-bottom-style: solid; color: black; }
    ul { margin: 0 0 32px 17px; }
    #objective { width: 500px; float: left; }
    #objective p { font-family: Georgia, Serif; font-style: italic; color: #666; }
    dt { font-style: italic; font-weight: bold; font-size: 18px; text-align: right; padding: 0 26px 0 0; width: 150px; float: left; height: 100px; border-right: 1px solid #999;  }
    dd { width: 600px; float: right; }
    dd.clear { float: none; margin: 0; height: 15px; }
</style>
</head>

<body>
    <div id="page-wrap">
        <div id="header" style="float: left;">
            <h1>
                <table>
                    <tr>
                        <td><img src="{{storage_path().'/app/public/logo_white.png'}}" style="height: 60px; width: 60px;"></td>
                        <td>
                            <div style="height: 70px; line-height: 70px; text-align: center; vertical-align: center; float: left; margin-left: 10px;">
                    <h2>Portofolio Mahasiswa</h2>
                </div>
                        </td>
                    </tr>
                </table>
            </h1>
        </div>
        <div class="clear"></div>
        <hr>
        <div id="contact-info" class="vcard" style="clear: both;">
            <!-- Microformats! -->
            <img src="{{storage_path().'/app/public/profil_example.jpg'}}" alt="" id="pic" style="height:150px; clear: both;" />
            <br>
            <h3 class="fn">{{$data->nama}}</h3>
            <h3 class="fn">{{$data->nim}}</h3>
            <br>
            <div class="clear"></div>
            <hr>
            <table style="text-align: left;">
                <tr>
                    <th style="width: 200px;">Tempat Tanggal Lahir</th>
                    <td>{{$data->tempat_lahir.", ".$data->tanggal_lahir}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Alamat</th>
                    <td>{{$data->alamat}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Email</th>
                    <td>{{$data->email}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Handphone</th>
                    <td>{{$data->handphone}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Jenis kelamin</th>
                    <td>{{$data->jenis_kelamin}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Agama</th>
                    <td>{{$data->agama}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Ketertarikan</th>
                    <td>{{$data->ketertarikan}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Motto</th>
                    <td><div id="objective">
                        <p>
                            {{$data->motto}}
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
    <dl>
        <dd class="clear"></dd>
        <dt>Riwayat Pendidikan</dt>
        <dd>
            <ul>
              @foreach($data->riwayat_pendidikan as $p)
              <li><b>{{$p->instansi}}</b>{{", Tahun ".$p->tahun_masuk."-".$p->tahun_lulus}}</li>
              @endforeach
          </ul>
      </dd>

      <dd class="clear"></dd>

      <dt>Prestasi</dt>
      <dd>
        <ul>
          @foreach($data->prestasi as $p)
          <li>
            <b>{{$p->tingkat}} | {{$p->kejuaraan.", ".$p->nama}}</b><br>
            <span class="desc">{{$p->tempat." ".$p->penyelenggara.", ".$p->tahun}} </span>
        </li>
        @endforeach
    </ul>
</dd>

<dd class="clear"></dd>

<dt>Sertifikasi & Keahlian</dt>
<dd>
    <ul>
      @foreach($data->sertifikasi as $p)
      <li>
        <b>{{$p->nama}} | {{$p->lembaga.", ".$p->tahun}}</b><br>
        <span class="desc">No Sertifikat {{$p->no_sertifikat}} </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Karya</dt>
<dd>
    <ul>
      @foreach($data->karya as $p)
      <li>
        <b>{{$p->nama.", ".$p->tahun}}</b><br>
        <span class="desc">
            Sebagai {{$p->jabatan}}<br>
            {{$p->deskripsi}}
        </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Pengalaman Organisasi</dt>
<dd>
    <ul>
      @foreach($data->organisasi as $p)
      <li>
        <b>{{$p->nama.", ".$p->periode}}</b><br>
        <span class="desc">Jabatan {{$p->jabatan}} </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Pengalaman Kerja</dt>
<dd>
    <ul>
      @foreach($data->pengalaman_kerja as $p)
      <li>
        <b>{{$p->instansi.", ".$p->periode}}</b><br>
        <span class="desc">Jabatan {{$p->jabatan}} </span>
    </li>
    @endforeach
</ul>
</dd>

<dd class="clear"></dd>
</dl>

<div class="clear"></div>

</div>

</body>

</html>