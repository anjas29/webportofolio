@extends('layouts.mahasiswa_layout')
@section('css')
<link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Profil
    </h1>
    <ol class="breadcrumb">
      <li><a href="/mahasiswa/profil"><i class="fa fa-home"></i> Profil</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-5">
        <!-- Profile Image -->
        <div class="box box-danger">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive" src="/images/mahasiswa/{{$profil->foto}}" alt="User profile picture">

            <h3 class="profile-username text-center">{{$profil->nama}}</h3>

            <p class="text-muted text-center">{{$profil->nim}}</p>
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-7">
        <form action="/mahasiswa/profil" method="post" enctype="multipart/form-data">
          <div class="box box-danger">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="padding-right: 20px; padding-left: 20px;">
                <div class="form-group">
                  <label class="text text-sm">Nama</label>  
                  <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{$profil->nama}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Email</label>  
                  <input type="text" class="form-control" placeholder="Email" name="email" value="{{$profil->email}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Alamat</label>  
                  <input type="text" class="form-control" placeholder="Alamat" name="alamat" value="{{$profil->alamat}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Tempat Lahir</label>  
                  <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" value="{{$profil->tempat_lahir}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Tanggal Lahir</label>  
                  <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tanggal_lahir" value="{{$profil->tanggal_lahir}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Foto</label>  
                  <input type="file" class="form-control" placeholder="Foto" name="foto">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Jenis Kelamin</label>  
                  <select class="form-control" name="jenis_kelamin">
                    @if($profil->jenis_kelamin == 'Laki-laki')
                      <option value="Laki-laki" selected>Laki-laki</option>
                      <option value="Perempuan">Perempuan</option>
                    @else
                      <option value="Laki-laki">Laki-laki</option>
                      <option value="Perempuan" selected>Perempuan</option>
                    @endif
                  </select>
                </div>
                <div class="form-group">
                  <label class="text text-sm">Agama</label>  
                  <input type="text" class="form-control" placeholder="Agama" name="agama" value="{{$profil->agama}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">No Telepon</label>  
                  <input type="text" class="form-control" placeholder="No Telepon" name="handphone" value="{{$profil->handphone}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Ketertarikan</label>  
                  <input type="text" class="form-control" placeholder="Ketertarikan" name="ketertarikan" value="{{$profil->ketertarikan}}">
                </div>
                <div class="form-group">
                  <label class="text text-sm">Motto</label>  
                  <input type="text" class="form-control" placeholder="Motto" name="motto" value="{{$profil->motto}}">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              {{csrf_field()}}
              <button class="btn btn-primary pull-right">Simpan</button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Row-->
    </div>
    <!-- End Row-->
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- detail News Modal -->


@endsection
@push('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
@endpush