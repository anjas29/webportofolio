@extends('layouts.mahasiswa_layout')
@push('css')
  <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sertifikat & Keahlian
      </h1>
      <ol class="breadcrumb">
        <li><a href="/mahasiswa/index"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="/mahasiswa/sertifikat">RSertifikat & Keahlian</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Sertifikat & Keahlian</h3>
              <button class="btn btn-sm btn-primary pull-right" data-toggle='modal' data-target="#createModal">Tambah Data</button>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th> 
                    <th>Lembaga</th>
                    <th>Tahun</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($data as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->nama}}</td>
                    <td>{{$d->lembaga}}</td>
                    <td>{{$d->tahun}}</td>
                    <td>{{$d->status}}</td>
                    <td>
                      <a href="#" class="edit btn btn-xs btn-primary" data-id='{{$d->id}}' data-nama='{{$d->nama}}' data-lembaga='{{$d->lembaga}}' data-tahun='{{$d->tahun}}' data-no_sertifikat='{{$d->no_sertifikat}}' data-image='{{$d->bukti_sertifikat}}' data-catatan='{{$d->catatan}}' data-nama-english='{{$d->nama_english}}'><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- crate Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='createModal'>
    <div class="modal-dialog" role="document">
      <form action="/mahasiswa/sertifikat" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Tambah Sertifikat & Keahlian</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="No Sertifikat (Opsional)" name="no_sertifikat">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Keahlian" name="nama">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Keahlian" name="nama_english">
                </div>
                <label class="text text-muted text-sm">Dalam bahasa Inggris</label>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Lembaga" name="lembaga">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun" name="tahun">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="file" name="bukti_sertifikat" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/mahasiswa/sertifikat" method="post" enctype="multipart/form-data" id="edit-form">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Sertifikat & Keahlian</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="No Sertifikat (Opsional)" name="no_sertifikat" id="detailNoSertifikat">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Keahlian" name="nama" id="detailNama">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Keahlian" name="nama_english" id="detailNamaEnglish">
                </div>
                <label class="text text-muted text-sm">Dalam bahasa Inggris</label>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Lembaga" name="lembaga" id="detailLembaga">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun" name="tahun" id="detailTahun">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="file" name="bukti_sertifikat" class="form-control" id="fileChooser">
                </div>
              </div>
              <div class="col-md-12">
                <div style="position: relative;">
                  <img src="" id="detailImage" class="img-responsive" style="margin-left: auto; margin-right: auto;" alt="Gambar">
                </div>
                <br>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailId">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  @endsection
  @push('js')
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });

      $('.edit').click(function(){
        var nama = $(this).data('nama');
        var no_sertifikat = $(this).data('no_sertifikat');
        var lembaga = $(this).data('lembaga');
        var tahun = $(this).data('tahun');
        var image = $(this).data('image');
        var namaEnglish = $(this).data('nama-english');

        var id = $(this).data('id');
        $("#edit-form").attr("action", "/mahasiswa/sertifikat/" + id);
        $('#detailNoSertifikat').val(no_sertifikat);
        $('#detailNama').val(nama);
        $('#detailNamaEnglish').val(namaEnglish);
        $('#detailLembaga').val(lembaga);
        $('#detailTahun').val(tahun);
        $('#detailImage').attr('src', '/images/sertifikat/'+ image);
        $('#detailId').val(id);
        $('#detailModal').modal();

      });
      $('.delete').click(function() {

        var id = $(this).data('id');

        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Apakaha anda ingin menghapus data ini</b>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/mahasiswa/sertifikat/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/mahasiswa/sertifikat/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });

      document.getElementById("fileChooser").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {

         $("#detailImage").fadeOut(1000, function() {
          document.getElementById("detailImage").src = e.target.result;
        }).fadeIn(1000);

       };

       reader.readAsDataURL(this.files[0]);
     };
    </script>
  @endpush

