@extends('layouts.mahasiswa_layout')
@section('css')
<link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb">
      <li><a href="/mahasiswa/index"><i class="fa fa-home"></i> Dashboard</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-5">
        <!-- Profile Image -->
        <div class="box box-danger">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive" src="/images/mahasiswa/{{$profil->foto}}" alt="User profile picture">

            <h3 class="profile-username text-center">{{$profil->nama}}</h3>

            <p class="text-muted text-center">{{$profil->nim}}</p>
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                 <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                 <a href="#" class="text-muted pull-right">{{$profil->email}}</a>
              </li>
              <li class="list-group-item">
                 <strong><i class="fa fa-phone margin-r-5"></i> No Telepon</strong>
                 <a href="#" class="text-muted pull-right">{{$profil->handphone}}</a>
              </li>
              <li class="list-group-item">
                 <strong><i class="fa fa-calendar margin-r-5"></i> TTL</strong>
                 <a href="#" class="text-muted pull-right">{{$profil->tempat_lahir}}, {{Carbon\Carbon::parse($profil->tanggal_lahir)->format('d F Y')}}</a>
              </li>
            </ul>
            <label class="text text-primary col-sm-12">
              Cetak Portofolio
              <a href="/mahasiswa/print/english" target="_blank" class="btn btn-success btn-sm pull-right" style="margin-left: 7px;"><i class="fa fa-language"></i> English</a>
              <a href="/mahasiswa/print/indonesian" target="_blank" class="btn btn-primary btn-sm pull-right"><i class="fa fa-language"></i> Indonesia</a>
            </label>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-7">
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Deskripsi</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <strong><i class="fa fa-graduation-cap margin-r-5"></i> Riwayat pendidikan</strong>

            <ul>
              @foreach($pendidikan as $p)
              <li><b>{{$p->instansi}}</b>{{", Tahun ".$p->tahun_masuk."-".$p->tahun_lulus}}</li>
              @endforeach
            </ul>

            <hr>

            <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>

            <p class="text-muted">{{$profil->alamat}}</p>

            <hr>

            <strong><i class="fa fa-pencil margin-r-5"></i> Ketertarikan</strong>

            <p>
              <span class="label label-danger">{{$profil->ketertarikan}}</span>
            <hr>

            <strong><i class="fa fa-file-text-o margin-r-5"></i> Motto</strong>

            <blockquote><i>{{$profil->motto}}</i></blockquote>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-trophy"></i></span>

              <div class="info-box-content">
                <span class='info-box-text'>Prestasi</span>
                <span class="info-box-number">{{$prestasi}}</span>
                <span class="progress-description" style="margin-top: 12px;">
                  <a href="{{route('mahasiswa.prestasi.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="fa fa-paint-brush"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Sertifikat & Keahlian</span>
                <span class="info-box-number">{{$sertifikasi}}</span>
                <span class="progress-description" style="margin-top: 12px;">
                  <a href="{{route('mahasiswa.sertifikat.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

          <!-- /.col -->
          <!-- fix for small devices only -->
          <div class="clearfix visible-sm-block"></div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
              <span class="info-box-icon"><i class="fa fa-rocket"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Karya & Penelitian</span>
                <b>Karya Personal {{$karya}}</b><br>
                <b>Penelitian Ilmiah {{$penelitian}}</b>
                <span class="progress-description" style="margin-top: 5px;">
                  <a href="{{route('mahasiswa.penelitian.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="fa fa-sitemap"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pengalaman</span>
                <b>Organisasi {{$pekerjaan}}</b><br>
                <b>Pekerjaan {{$pekerjaan}}</b>
                <span class="progress-description" style="margin-top: 5px;">
                  <a href="{{route('mahasiswa.pekerjaan.index')}}" style="text-decorations:none; color:inherit;"><b>Selengkapnya</b></a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

          <!-- /.col -->
        </div>
      </div>
      <!-- End Row-->
    </div>
    <!-- End Row-->
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- detail News Modal -->


@endsection
@push('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
@endpush