@extends('layouts.mahasiswa_layout')
@push('css')
  <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penelitian Ilmiah
      </h1>
      <ol class="breadcrumb">
        <li><a href="/mahasiswa/index"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active"><a href="/mahasiswa/penelitian">Penelitian Ilmiah</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-warning'>
            <div class="box-header">
              <i class="fa fa-user-secret"></i>
              <h3 class="box-title">Penelitian Ilmiah</h3>
              <button class="btn btn-sm btn-primary pull-right" data-toggle='modal' data-target="#createModal">Tambah Data</button>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th> 
                    <th>Peran/Jabatan</th>
                    <th>Pembimbing</th>
                    <th>Tahun</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>                  
                </thead>
                <tbody>
                  @foreach($data as $i =>  $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->nama}}</td>
                    <td>{{$d->jabatan}}</td>
                    <td>{{$d->pembimbing}}</td>
                    <td>{{$d->tahun}}</td>
                    <td>{{$d->status}}</td>
                    <td>
                      <a href="#" class="edit btn btn-xs btn-primary" data-id='{{$d->id}}' data-nama='{{$d->nama}}' data-deskripsi='{{$d->deskripsi}}' data-tahun='{{$d->tahun}}' data-jabatan='{{$d->jabatan}}' data-pembimbing='{{$d->pembimbing}}' data-image='{{$d->bukti_karya}}' data-catatan='{{$d->catatan}}' data-nama-english='{{$d->nama_english}}' data-jabatan-english='{{$d->jabatan_english}}'><i class="fa fa-pencil"></i></a>
                      <a href="#" class="delete btn btn-xs btn-danger" data-id='{{$d->id}}'><i class="fa fa-times"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- create Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='createModal'>
    <div class="modal-dialog" role="document">
      <form action="/mahasiswa/penelitian" method="post" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Tambah Penelitian Ilmiah</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Penelitian" name="nama">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Penelitian" name="nama_english">
                </div>
                <label class="text text-sm text-muted">Dalam bahasa Inggris</label>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <textarea name="deskripsi" class="form-control" placeholder="Deskripsi"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Peran/Jabatan" name="jabatan">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Peran/Jabatan" name="jabatan_english">
                </div>
                <label class="text text-sm text-muted">Dalam bahasa Inggris</label>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Pembimbing" name="pembimbing" >
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun" name="tahun">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="file" name="bukti_karya" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!--Detail Class Modal -->
  <div class="modal fade" tabindex="-1" role="dialog" id='detailModal'>
    <div class="modal-dialog" role="document">
      <form action="/mahasiswa/penelitian" method="post" enctype="multipart/form-data" id="edit-form">
        <div class="modal-content">
          <div class="modal-header">
            <strong>Detail Penelitian Ilmiah</strong><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama penelitian" name="nama" id="detailNama">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Penelitian" name="nama_english" id="detailNamaEnglish">
                </div>
                <label class="text text-sm text-muted">Dalam bahasa Inggris</label>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <textarea name="deskripsi" class="form-control" placeholder="Deskripsi" id="detailDeksripsi"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Peran/Jabatan" name="jabatan" id="detailJabatan">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Peran/Jabatan" name="jabatan_english" id="detailJabatanEnglish">
                </div>
                <label class="text text-sm text-muted">Dalam bahasa Inggris</label>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Pembimbing" name="pembimbing" id="detailPembimbing">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="text" class="form-control" placeholder="Tahun" name="tahun" id="detailTahun">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-feed"></i></span>
                  <input type="file" name="bukti_karya" class="form-control" id="fileChooser">
                </div>
              </div>
              <div class="col-md-12">
                <div style="position: relative;">
                  <img src="" id="detailImage" class="img-responsive" style="margin-left: auto; margin-right: auto;" alt="Gambar">
                </div>
                <br>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              {{csrf_field()}}
               {!! method_field('patch') !!}
              <input type="hidden" name="_id" value="" id="detailId">
              <button type="button" class="btn btn-sm" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  @endsection
  @push('js')
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
    <script>
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });

      $('.edit').click(function(){
        var nama = $(this).data('nama');
        var deskripsi = $(this).data('deskripsi');
        var pembimbing = $(this).data('pembimbing');
        var jabatan = $(this).data('jabatan');
        var tahun = $(this).data('tahun');
        var image = $(this).data('image');
        var namaEnglish = $(this).data('nama-english');
        var jabatanEnglish = $(this).data('jabatan-english');

        var id = $(this).data('id');
        $("#edit-form").attr("action", "/mahasiswa/penelitian/" + id);
        $('#detailNama').val(nama);
        $('#detailNamaEnglish').val(namaEnglish);
        $('#detailDeksripsi').html(deskripsi);
        $('#detailJabatan').val(jabatan);
        $('#detailJabatanEnglish').val(jabatanEnglish);
        $('#detailPembimbing').val(pembimbing);
        $('#detailTahun').val(tahun);
        $('#detailImage').attr('src', '/images/penelitian/'+ image);
        $('#detailId').val(id);
        $('#detailModal').modal();

      });
      $('.delete').click(function() {

        var id = $(this).data('id');

        var _method = 'delete';
        var _token = '{{csrf_token()}}';

        bootbox.confirm("<b>Apakah anda ingin menghapus data ini</b>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Process...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/mahasiswa/penelitian/"+id, {id: id, _token:_token, _method:_method})
            .done(function(result) {
              window.location.replace("/mahasiswa/penelitian/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Server Error! Please reload this page again.');
            });
          };
        });
      });

      document.getElementById("fileChooser").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {

               $("#detailImage").fadeOut(1000, function() {
                    document.getElementById("detailImage").src = e.target.result;
                }).fadeIn(1000);
              
          };

          reader.readAsDataURL(this.files[0]);
      };
    </script>
  @endpush

