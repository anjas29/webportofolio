<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Portofolio Mahasiswa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!-- <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}"> -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <!-- <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}"> -->
  <style type="text/css">
      .clear { float: none; margin: 0; height: 15px; }
  </style>
  <style type="text/css">
    * { margin: 0; padding: 0; }
    body { font: 16px Helvetica, Sans-Serif; line-height: 24px; background: url(/images/noise.jpg); }
    .clear { clear: both; }
    #page-wrap { width: 850px; margin: 5px auto 60px; }
    #pic { float: right; margin: 10px 30px 0 0; }
    h1 { margin: 0 0 16px 0; padding: 0 0 16px 0; font-size: 42px; font-weight: bold; letter-spacing: -2px;  }
    h2 { font-size: 20px; margin: 0 0 6px 0; position: relative; }
    h2 span { position: absolute; bottom: 0; right: 0; font-style: italic; font-family: Georgia, Serif; font-size: 16px; color: #999; font-weight: normal; }
    .desc { bottom: 0; right: 0; font-style: italic; font-family: Georgia, Serif; font-size: 16px; color: #999; font-weight: normal; }
    p { margin: 0 0 16px 0; }
    a { color: #999; text-decoration: none; border-bottom: 1px dotted #999; }
    a:hover { border-bottom-style: solid; color: black; }
    ul { margin: 0 0 32px 17px; }
    #objective { width: 500px; float: left; }
    #objective p { font-family: Georgia, Serif; font-style: italic; color: #666; }
    dt { font-style: italic; font-weight: bold; font-size: 18px; text-align: right; padding: 0 26px 0 0; width: 150px; float: left; height: 100px; border-right: 1px solid #999;  }
    dd { width: 600px; float: right; }
    dd.clear { float: none; margin: 0; height: 15px; }
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
          <div class='col-sm-12' style="background: #4CAC9D; height: 90px;">
            <img src='/images/logo_white.png' class="img-responsive" style="height: 70px; padding: 10px 10px 10px 20px; float: left; ">
            <h1 style="color: #fff; font-size: 30px; line-height: 65px; vertical-align: center; float: left; width: 100%; text-align: center; position: absolute;">
                Student Portofolio
            </h1>
            <h1 style="color: #fff; font-size: 20px; line-height: 120px; vertical-align: center; float: left; width: 100%; text-align: center; position: absolute;">
                Universitas Negeri Yogyakarta
            </h1>
        </div>
    </div>
    <div class="row">
        <div id="page-wrap">
        <div id="contact-info" class="vcard" style="clear: both;">
            <!-- Microformats! -->
            <img src="/images/profil_example.jpg" alt="" id="pic" style="height:150px; clear: both;" />
            <br>
            <h3 class="fn" style="padding: 20px 0 0 30px; font-size: 25px;">
                {{$data->nama}}
                <br>
            </h3>
            <h4 style="padding: 5px 0 10px 30px; font-size: 20px;"s><i>{{$data->nim}}</i></h4>
            <table style="text-align: left; margin: 10px 5px 5px 30px;">
                <tr>
                    <th style="width: 200px;">Place and date of birth</th>
                    <td>{{$data->tempat_lahir.", ".Carbon\Carbon::parse($data->tanggal_lahir)->format('d F Y')}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Address</th>
                    <td>{{$data->alamat}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Email</th>
                    <td>{{$data->email}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Phone Number</th>
                    <td>{{$data->handphone}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Gender</th>
                    <td>{{$data->jenis_kelamin}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Religion</th>
                    <td>{{$data->agama}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Interest</th>
                    <td>{{$data->ketertarikan}}</td>
                </tr>
                <tr>
                    <th style="width: 200px;">Motto</th>
                    <td><div id="objective">
                        <p>
                            {{$data->motto}}
                        </p>
                    </div>
                    </td>
                </tr>
            </table>
            <div class="clear"></div>
            <hr>
        <hr>
    </div>
    <div class="clear"></div>
    <dl>
        <dd class="clear"></dd>
        <dt>Educations</dt>
        <dd>
            <ul>
              @foreach($data->riwayat_pendidikan as $p)
              <li><b>{{$p->instansi}}</b>{{", Period ".$p->tahun_masuk."-".$p->tahun_lulus}}</li>
              @endforeach
          </ul>
      </dd>

      <dd class="clear"></dd>

      <dt>Achievments</dt>
      <dd>
        <ul>
          @foreach($data->prestasi as $p)
          <li>
            <?php $tingkat = 'International'; ?>
            @if($p->tingkat == 'Lokal')
              <?php $tingkat = 'Local'; ?>
            @elseif($p->tingkat == 'Nasional')
              <?php $tingkat = 'National'; ?>
            @elseif($p->tingkat == 'Regional')
              <?php $tingkat = 'Regional'; ?>
            @elseif($p->tingkat == 'Internasional')
              <?php $tingkat = 'International'; ?>
            @endif

            @if($p->kejuaraan == 'Juara 1')
              <?php $kejuaraan = '1st Place'; ?>
            @elseif($p->kejuaraan == 'Juara 2')
              <?php $kejuaraan = '2nd Place'; ?>
            @elseif($p->kejuaraan == 'Juara 3')
              <?php $kejuaraan = '3rd Place'; ?>
            @endif
            <b>{{$tingkat}} | {{$kejuaraan.", ".$p->nama_english}}</b><br>
            <span class="desc">{{$p->tempat." ".$p->penyelenggara.", ".$p->tahun}} </span>
        </li>
        @endforeach
    </ul>
</dd>

<dd class="clear"></dd>

<dt>Certifications & Expertises</dt>
<dd>
    <ul>
      @foreach($data->sertifikasi as $p)
      <li>
        <b>{{$p->nama_english}} | {{$p->lembaga.", ".$p->tahun}}</b><br>
        <span class="desc">Certificate number {{$p->no_sertifikat}} </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Research</dt>
<dd>
    <ul>
      @foreach($data->penelitian as $p)
      <li>
        <b>{{$p->nama_english.", ".$p->tahun}}</b><br>
        <span class="desc">
            as {{$p->jabatan_english}}<br>
        </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Projects</dt>
<dd>
    <ul>
      @foreach($data->karya as $p)
      <li>
        <b>{{$p->nama_english.", ".$p->tahun}}</b><br>
        <span class="desc">
            as {{$p->jabatan_english}}<br>
        </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Organizational experiences</dt>
<dd>
    <ul>
      @foreach($data->organisasi as $p)
      <li>
        <b>{{$p->nama.", ".$p->periode}}</b><br>
        <span class="desc">as {{$p->jabatan_english}} </span>
    </li>
    @endforeach
</ul>
</dd>
<dd class="clear"></dd>

<dt>Working experiences</dt>
<dd>
    <ul>
      @foreach($data->pengalaman_kerja as $p)
      <li>
        <b>{{$p->instansi.", ".$p->periode}}</b><br>
        <span class="desc">as {{$p->jabatan_english}} </span>
    </li>
    @endforeach
</ul>
</dd>

<dd class="clear"></dd>
</dl>

<div class="clear"></div>

</div>
    </div>
      <!-- /.row -->
    </section>
  <script src="{{ asset('/adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/adminlte/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},1000);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
