<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePenelitian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penelitian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mahasiswa_id')->unsigned();
            $table->string('nama')->nullable();
            $table->string('nama_english')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('bukti_karya')->nullable();
            $table->string('tahun')->nullable();
            $table->string('pembimbing')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('jabatan_english')->nullable();
            $table->string('status');
            $table->string('catatan');
            $table->integer('periode_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penelitian');
    }
}
