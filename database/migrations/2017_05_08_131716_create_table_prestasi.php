<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrestasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mahasiswa_id')->unsigned();
            $table->string('nama');
            $table->string('nama_english')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('kejuaraan');
            $table->string('penyelenggara')->nullable();
            $table->string('tingkat')->nullable();
            $table->string('bidang')->nullable();
            $table->date('waktu')->nullable();
            $table->string('tahun')->nullable();
            $table->string('bukti_kegiatan')->nullable();
            $table->string('bukti_sertifikat')->nullable();
            $table->string('status');
            $table->string('catatan');
            $table->integer('periode_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestasi');
    }
}
