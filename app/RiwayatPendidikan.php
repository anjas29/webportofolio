<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model
{
    protected $table = 'riwayat_pendidikan';

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa');
    }

    public function periode()
    {
    	return $this->belongsTo('App\Periode');
    }
}
