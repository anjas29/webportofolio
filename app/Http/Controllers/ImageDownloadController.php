<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use File;
use Response;

class ImageDownloadController extends Controller
{
	public function getImageRiwayatPendidikan($filename)
	{
		$path = storage_path() . '/app/pendidikan/bukti/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImageKegiatanPrestasi($filename)
	{
		$path = storage_path() . '/app/prestasi/kegiatan/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImageSertifikatPrestasi($filename)
	{
		$path = storage_path() . '/app/prestasi/sertifikat/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImageSertifikat($filename)
	{
		$path = storage_path() . '/app/sertifikat/bukti/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImageKarya($filename)
	{
		$path = storage_path() . '/app/karya/bukti/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImagePenelitian($filename)
	{
		$path = storage_path() . '/app/penelitian/bukti/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImageOrganisasi($filename)
	{
		$path = storage_path() . '/app/organisasi/bukti/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}

	public function getImagePekerjaan($filename)
	{
		$path = storage_path() . '/app/pekerjaan/bukti/' . $filename;

		if(!File::exists($path)) return abort('404');

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}
}
