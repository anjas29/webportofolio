<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminFakultas;
use App\Fakultas;
use Hash;
class AdminFakultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AdminFakultas::with(array('fakultas' => function ($query){
                                    $query->orderBy('nama', 'ASC');
                                }))->get();
        $fakultas = Fakultas::orderBy('nama', 'ASC')->get();

        return view('admin.admin_fakultas.index')->withData($data)->withFakultas($fakultas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new AdminFakultas;
        $data->nama = $request->input('nama');
        $data->fakultas_id = $request->input('fakultas_id');
        $data->username = $request->input('username');
        $data->password = Hash::make($request->input('username'));
        $data->save();

        return redirect(route('admin.admin_fakultas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = AdminFakultas::where('id', $id)->firstOrFail();
        $data->nama = $request->input('nama');
        $data->fakultas_id = $request->input('fakultas_id');
        $data->username = $request->input('username');
        $data->password = Hash::make($request->input('username'));
        $data->save();

        return redirect(route('admin.admin_fakultas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AdminFakultas::where('id', $id)->firstOrFail();
        $data->delete();

        return redirect(route('admin.admin_fakultas.index'));
    }
}
