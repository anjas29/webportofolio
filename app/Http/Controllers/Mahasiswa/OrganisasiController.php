<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Organisasi;
use App\Periode;
use Carbon\Carbon;

class OrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $userId = auth('mahasiswa')->user()->id;
        $data = Organisasi::whereHas('mahasiswa', function($query) use ($userId)
                        {
                            $query->where('id', $userId);
                        })->get();

        return view('mahasiswa.organisasi.index')->withData($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = new Organisasi;
        $data->mahasiswa_id = $userId;
        $data->nama = $request->input('nama');
        $data->jabatan = $request->input('jabatan');
        $data->jabatan_english = $request->input('jabatan_english');
        $data->periode = $request->input('periode');
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_surat_keterangan')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_surat_keterangan'), 'Bukti_Surat_Keterangan_','/app/organisasi/bukti');

            $data->bukti_surat_keterangan = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.organisasi.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = Organisasi::where('id', $id)->firstOrFail();
        $data->mahasiswa_id = $userId;
        $data->nama = $request->input('nama');
        $data->jabatan = $request->input('jabatan');
        $data->jabatan_english = $request->input('jabatan_english');
        $data->periode = $request->input('periode');
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_surat_keterangan')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_surat_keterangan'), 'Bukti_Surat_Keterangan_','/app/organisasi/bukti');

            $data->bukti_surat_keterangan = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.organisasi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();

        $data = Organisasi::where('id', $id)->firstOrFail();
        $data->delete();
        return redirect(route('mahasiswa.organisasi.index'));
    }

    public function uploadFile($id, $file, $tipeDokumen, $path){
            $path = storage_path().$path;

            $extension = $file->getClientOriginalExtension();

            if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Tidak Valid');
                    return redirect(route('mahasiswa.organisasi.index'));
                }

            $fileName = $tipeDokumen.$id.'_'.Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;

            $file->move($path, $fileName);

            return $fileName;
    }
}
