<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RiwayatPendidikan;
use App\Periode;
use Carbon\Carbon;

class RiwayatPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $userId = auth('mahasiswa')->user()->id;
        $data = RiwayatPendidikan::whereHas('mahasiswa', function($query) use ($userId)
                        {
                            $query->where('id', $userId);
                        })->get();

        return view('mahasiswa.pendidikan.index')->withData($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = new RiwayatPendidikan;
        $data->mahasiswa_id = $userId;
        $data->instansi = $request->input('instansi');
        $data->jenjang = $request->input('jenjang');
        $data->tahun_masuk = $request->input('tahun_masuk');
        $data->tahun_lulus = $request->input('tahun_lulus');
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_ijazah')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_ijazah'), 'Bukti_Ijazah_','/app/pendidikan/bukti');

            $data->bukti_ijazah = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.pendidikan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = RiwayatPendidikan::where('id', $id)->firstOrFail();
        $data->mahasiswa_id = $userId;
        $data->instansi = $request->input('instansi');
        $data->jenjang = $request->input('jenjang');
        $data->tahun_masuk = $request->input('tahun_masuk');
        $data->tahun_lulus = $request->input('tahun_lulus');
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_ijazah')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_ijazah'), 'Bukti_Ijazah_','/app/pendidikan/bukti');

            $data->bukti_ijazah = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.pendidikan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();

        $data = RiwayatPendidikan::where('id', $id)->firstOrFail();
        $data->delete();
        return redirect(route('mahasiswa.pendidikan.index'));
    }

    public function uploadFile($id, $file, $tipeDokumen, $path){
            $path = storage_path().$path;

            $extension = $file->getClientOriginalExtension();

            if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Tidak Valid');
                    return redirect(route('mahasiswa.pendidikan.index'));
                }

            $fileName = $tipeDokumen.$id.'_'.Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;

            $file->move($path, $fileName);

            return $fileName;
    }
}