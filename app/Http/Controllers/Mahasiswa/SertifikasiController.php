<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sertifikasi;
use App\Periode;
use Carbon\Carbon;

class SertifikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $userId = auth('mahasiswa')->user()->id;
        $data = Sertifikasi::whereHas('mahasiswa', function($query) use ($userId)
                        {
                            $query->where('id', $userId);
                        })->get();

        return view('mahasiswa.sertifikat.index')->withData($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = new Sertifikasi;
        $data->nama = $request->input('nama');
        $data->nama_english = $request->input('nama_english');
        $data->mahasiswa_id = $userId;
        $data->no_sertifikat = $request->input('no_sertifikat');
        $data->lembaga = $request->input('lembaga');
        $data->tahun = $request->input('tahun');
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_sertifikat')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_sertifikat'), 'Bukti_Sertifikat_','/app/sertifikat/bukti');

            $data->bukti_sertifikat = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.sertifikat.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = Sertifikasi::where('id', $id)->firstOrFail();
        $data->nama = $request->input('nama');
        $data->nama_english = $request->input('nama_english');
        $data->mahasiswa_id = $userId;
        $data->no_sertifikat = $request->input('no_sertifikat');
        $data->lembaga = $request->input('lembaga');
        $data->tahun = $request->input('tahun');
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_sertifikat')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_sertifikat'), 'Bukti_Sertifikat_','/app/sertifikat/bukti');

            $data->bukti_sertifikat = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.sertifikat.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();

        $data = Sertifikasi::where('id', $id)->firstOrFail();
        $data->delete();
        return redirect(route('mahasiswa.sertifikat.index'));
    }

    public function uploadFile($id, $file, $tipeDokumen, $path){
            $path = storage_path().$path;

            $extension = $file->getClientOriginalExtension();

            if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Tidak Valid');
                    return redirect(route('mahasiswa.sertifikat.index'));
                }

            $fileName = $tipeDokumen.$id.'_'.Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;

            $file->move($path, $fileName);

            return $fileName;
    }
}