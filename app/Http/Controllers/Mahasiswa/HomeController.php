<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Karya;
use App\PengalamanKerja;
use App\RiwayatPendidikan;
use App\Prestasi;
use App\Organisasi;
use App\Sertifikasi;
use App\Mahasiswa;
use App\Penelitian;
use Carbon\Carbon;

use PDF;

class HomeController extends Controller
{
    public function getIndex()
    {	
    	$profil = auth('mahasiswa')->user();
    	$user_id = $profil->id;

    	$pengalamanKerja = PengalamanKerja::where('mahasiswa_id', $user_id)->count();
    	$prestasi = Prestasi::where('mahasiswa_id', $user_id)->count();
    	$organisasi = Organisasi::where('mahasiswa_id', $user_id)->count();
    	$pendidikan = RiwayatPendidikan::where('mahasiswa_id', $user_id)->get();
    	$riwayatPendidikan = $pendidikan->count();
    	$karya = Karya::where('mahasiswa_id', $user_id)->count();
    	$sertifikasi = Sertifikasi::where('mahasiswa_id', $user_id)->count();
        $penelitian = Penelitian::where('mahasiswa_id', $user_id)->count();


    	return view('mahasiswa.dashboard')
    					->withProfil($profil)
    					->withPekerjaan($pengalamanKerja)
    					->withRiwayatPendidikan($riwayatPendidikan)
    					->withPendidikan($pendidikan)
    					->withPrestasi($prestasi)
    					->withKarya($karya)
                        ->withPenelitian($penelitian)
    					->withSertifikasi($sertifikasi)
    					->withOrganisasi($organisasi);
    }

    public function getProfil()
    {
        $profil = auth('mahasiswa')->user();

        return view('mahasiswa.profil')->withProfil($profil);
    }

    public function postProfil(Request $request)
    {
        $id = auth('mahasiswa')->user()->id;
        $data = Mahasiswa::where('id', $id)->firstOrFail();

        $data->nama = $request->input('nama');
        $data->alamat = $request->input('alamat');
        $data->email = $request->input('email');
        $data->handphone = $request->input('handphone');
        $data->tempat_lahir = $request->input('tempat_lahir');
        $data->tanggal_lahir = $request->input('tanggal_lahir');
        $data->motto = $request->input('motto');
        $data->jenis_kelamin = $request->input('jenis_kelamin');
        $data->agama = $request->input('agama');
        $data->ketertarikan = $request->input('ketertarikan');
        
        if($request->hasFile('foto')){
            $fileName = $this->uploadFile($id, $request->file('foto'), 'Foto','/app/mahasiswa');

            $data->foto = $fileName;
        }

        $data->save();
        return redirect('/mahasiswa/index');

    }
    public function getPrint($lang){
        $user = auth('mahasiswa')->user();
        $user_id = $user->id;
    	$data = Mahasiswa::where('id', $user_id)
                            ->with('riwayat_pendidikan')
                            ->with('prestasi')
                            ->with('pengalaman_kerja')
                            ->with('karya')
                            ->with('organisasi')
                            ->with('sertifikasi')
                            ->with('penelitian')
                            ->first();

        // $pdf = PDF::loadView('mahasiswa.print.indonesian', compact('data'));
        if($lang == 'english')
            return view('mahasiswa.print.english')->withData($data);
        else
            return view('mahasiswa.print.indonesian')->withData($data);
        // return view('print.portofolio')->withData($data);
        // return $pdf->download($user->nim.'.pdf');
    }

    public function uploadFile($id, $file, $tipeDokumen, $path){
            $path = storage_path().$path;

            $extension = $file->getClientOriginalExtension();

            if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Tidak Valid');
                    return redirect(route('mahasiswa.karya.index'));
            }

            $fileName = $tipeDokumen.$id.'_'.Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;

            $file->move($path, $fileName);

            return $fileName;
    }
}

