<?php

namespace App\Http\Controllers\Mahasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Prestasi;
use App\Periode;
use Carbon\Carbon;

class PrestasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $userId = auth('mahasiswa')->user()->id;
        $data = Prestasi::whereHas('mahasiswa', function($query) use ($userId)
                        {
                            $query->where('id', $userId);
                        })->get();

        return view('mahasiswa.prestasi.index')->withData($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = new Prestasi;
        $data->mahasiswa_id = $userId;
        $data->nama = $request->input('nama');
        $data->nama_english = $request->input('nama_english');
        $data->deskripsi = $request->input('deskripsi');
        $data->kejuaraan = $request->input('kejuaraan');
        $data->penyelenggara = $request->input('penyelenggara');
        $data->tingkat = $request->input('tingkat');
        $data->bidang = $request->input('bidang');
        $data->waktu = $request->input('waktu');
        $data->tahun = $periodeId->tahun;
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_sertifikat')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_sertifikat'), 'Bukti_Prestasi_','/app/prestasi/sertifikat');

            $data->bukti_sertifikat = $fileName;
        }

        if($request->hasFile('bukti_kegiatan')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_kegiatan'), 'Bukti_Kegiatan_','/app/prestasi/kegiatan');

            $data->bukti_kegiatan = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.prestasi.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();
        $data = Prestasi::where('id', $id)->firstOrFail();
        $data->mahasiswa_id = $userId;
        $data->nama = $request->input('nama');
        $data->nama_english = $request->input('nama_english');
        $data->deskripsi = $request->input('deskripsi');
        $data->kejuaraan = $request->input('kejuaraan');
        $data->penyelenggara = $request->input('penyelenggara');
        $data->tingkat = $request->input('tingkat');
        $data->bidang = $request->input('bidang');
        $data->waktu = $request->input('waktu');
        $data->tahun = $periodeId->tahun;
        
        $data->status = 'Tahap Konfirmasi';
        $data->catatan = 'Tahap konfirmasi administrator';

        if($periodeId != null)
            $data->periode_id = $periodeId->id;
        else
            $data->periode_id = null;
        
        if($request->hasFile('bukti_sertifikat')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_sertifikat'), 'Bukti_Prestasi_','/app/prestasi/sertifikat');

            $data->bukti_sertifikat = $fileName;
        }

        if($request->hasFile('bukti_kegiatan')){
            $fileName = $this->uploadFile($userId, $request->file('bukti_kegiatan'), 'Bukti_Kegiatan_','/app/prestasi/kegiatan');

            $data->bukti_kegiatan = $fileName;
        }

        $data->save();
        return redirect(route('mahasiswa.prestasi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = auth('mahasiswa')->user()->id;
        $periodeId = Periode::where('status', 1)->first();

        $data = Prestasi::where('id', $id)->firstOrFail();
        $data->delete();
        return redirect(route('mahasiswa.prestasi.index'));
    }

    public function uploadFile($id, $file, $tipeDokumen, $path){
            $path = storage_path().$path;

            $extension = $file->getClientOriginalExtension();

            if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Tidak Valid');
                    return redirect(route('mahasiswa.prestasi.index'));
                }

            $fileName = $tipeDokumen.$id.'_'.Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;

            $file->move($path, $fileName);

            return $fileName;
    }
}