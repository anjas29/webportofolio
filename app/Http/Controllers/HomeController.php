<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Karya;
use App\PengalamanKerja;
use App\RiwayatPendidikan;
use App\Prestasi;
use App\Organisasi;
use App\Sertifikasi;
use App\Mahasiswa;
use App\Penelitian;

use Session;

class HomeController extends Controller
{
    public function getIndex()
    {   
        $pengalamanKerja = PengalamanKerja::count();
        $prestasi = Prestasi::count();
        $organisasi = Organisasi::count();
        $pendidikan = RiwayatPendidikan::count();
        $karya = Karya::count();
        $sertifikasi = Sertifikasi::count();
        $penelitian = Penelitian::count();


        return view('admin.dashboard')
                        ->withPekerjaan($pengalamanKerja)
                        ->withPendidikan($pendidikan)
                        ->withPrestasi($prestasi)
                        ->withKarya($karya)
                        ->withPenelitian($penelitian)
                        ->withSertifikasi($sertifikasi)
                        ->withOrganisasi($organisasi);
    }

    public function getLogin(){

        if (auth('users')->check()) {
            return redirect('/admin');
        }else if(auth('mahasiswa')->check()){
            return redirect('/mahasiswa');
        }
        
		return view('login');
	}

    public function postLogin(Request $request){
    	$authSutdent = auth('mahasiswa');
    	$authAdmin = auth('users');

        $credential = array(
            'username' => $request->input('username'),
            'password' => $request->input('password')
        );

        if($authSutdent->attempt($credential)){
            Session::flash('loginSuccess', '');
            return redirect(route('mahasiswa.index'));
        }else if($authAdmin->attempt($credential)){
        	Session::flash('loginSuccess', '');
            return redirect(route('admin.index'));
        }else{
            Session::flash('loginFailed', '');
            return redirect(route('login'));
        }
        return redirect(route('login'));
    }

    public function getLogout(){
    	auth('mahasiswa')->logout();
    	auth('users')->logout();
    	return redirect(route('login'));
    }
}
