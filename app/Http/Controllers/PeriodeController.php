<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periode;

class PeriodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Periode::orderBy('tahun', 'ASC')->get();
        return view('admin.periode.index')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Periode;
        $data->tahun = $request->input('tahun');
        $data->status = 0;
        $data->save();

        return redirect(route('admin.periode.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Periode::where('id', $id)->firstOrFail();
        $data->tahun = $request->input('tahun');
        $data->save();

        return redirect(route('admin.periode.index'));
    }

    public function setAktifPeriode(Request $request){
        $id = $request->input('id');

        Periode::where('status', 1)->update(array('status'=> 0));

        $activeData = Periode::where('id', $id)->firstOrFail();
        $activeData->status = 1;
        $activeData->save();

        return redirect(route('admin.periode.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Periode::where('id', $id)->firstOrFail();
        $data->delete();

        return redirect(route('admin.periode.index'));
    }
}
