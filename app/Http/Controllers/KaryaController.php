<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karya;
class KaryaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Karya::all();
        return view('admin.karya.index')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Karya::where('id', $id)->firstOrFail();
        $data->status = $request->input('status');

        if($data->status == 'Diterima')
            $data->catatan = 'Pengajuan Diterima';
        elseif($data->status == 'Ditolak')
            $data->catatan = 'Pengajuan Ditolak, '.$request->input('catatan');
        else
            $data->catatan = 'Tahap Konfirmasi Administrator';

        $data->save();
        return redirect(route('admin.karya.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $data = Karya::where('id', $id)->firstOrFail();
        $data->delete();

        return redirect(route('admin.karya.index'));
    }
}
