<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Fakultas;
use App\Prodi;

use Hash;
class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswa = Mahasiswa::all();
        $fakultas = Fakultas::all();
        $prodi = Prodi::orderBy('nama', 'ASC')->get();

        return view('admin.mahasiswa.index')
                    ->withData($mahasiswa)
                    ->withProdi($prodi)
                    ->withFakultas($fakultas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Mahasiswa;
        $data->nama = $request->input('nama');
        $data->nim = $request->input('nim');
        $data->prodi_id = $request->input('prodi_id');
        $data->jenis_kelamin = $request->input('jenis_kelamin');
        $data->username = $request->input('nim');
        $data->password = Hash::make($request->input('nim'));
        $data->save();

        return redirect(route('admin.mahasiswa.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Mahasiswa::where('id', $id)->firstOrFail();
        $data->nama = $request->input('nama');
        $data->nim = $request->input('nim');
        $data->prodi_id = $request->input('prodi_id');
        $data->jenis_kelamin = $request->input('jenis_kelamin');
        $data->username = $request->input('nim');
        $data->password = Hash::make($request->input('nim'));
        $data->save();

        return redirect(route('admin.mahasiswa.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Mahasiswa::where('id', $id)->firstOrFail();
        $data->delete();

        return redirect(route('admin.mahasiswa.index'));
    }
}
