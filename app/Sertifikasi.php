<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sertifikasi extends Model
{
    protected $table = 'sertifikasi';

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa');
    }
    public function periode()
    {
    	return $this->belongsTo('App\Periode');
    }
}
