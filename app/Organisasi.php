<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    protected $table = 'organisasi';

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa');
    }
    public function detail_periode()
    {
    	return $this->belongsTo('App\Periode');
    }
}
