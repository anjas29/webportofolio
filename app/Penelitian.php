<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penelitian extends Model
{
    protected $table = 'penelitian';

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa');
    }

    public function periode()
    {
    	return $this->belongsTo('App\Periode');
    }
}
