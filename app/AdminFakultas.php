<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminFakultas extends Authenticatable
{
    protected $table = 'admin_fakultas';

    public function fakultas()
    {
    	return $this->belongsTo('App\Fakultas');
    }
}
