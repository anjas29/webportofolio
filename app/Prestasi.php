<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestasi extends Model
{
    protected $table = 'prestasi';

    public function mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa');
    }

    public function periode()
    {
    	return $this->belongsTo('App\Periode');
    }
}
