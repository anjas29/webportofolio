<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mahasiswa extends Authenticatable
{
    protected $table = 'mahasiswa';

    public function prodi()
    {
    	return $this->belongsTo('App\Prodi');
    }

    public function riwayat_pendidikan()
    {
    	return $this->hasMany('App\RiwayatPendidikan');
    }

    public function prestasi()
    {
    	return $this->hasMany('App\Prestasi');
    }

    public function organisasi()
    {
    	return $this->hasMany('App\Organisasi');
    }

    public function karya()
    {
    	return $this->hasMany('App\Karya');
    }

    public function pengalaman_kerja()
    {
    	return $this->hasMany('App\PengalamanKerja');
    }

    public function sertifikasi()
    {
        return $this->hasMany('App\Sertifikasi');
    }

    public function penelitian()
    {
        return $this->hasMany('App\Penelitian');
    }
}
