<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getLogin');

Route::get('login', 'HomeController@getLogin')->name('login');
Route::post('login', 'HomeController@postLogin')->name('login');
Route::get('logout', 'HomeController@getLogout')->name('logout');

Route::group(array('prefix'=>'admin', 'as' => 'admin.', 'middleware'=>'auth:users'), function(){
	Route::get('/', 'HomeController@getIndex');
	Route::get('index', 'HomeController@getIndex')->name('index');

    Route::resource('periode', 'PeriodeController');
    Route::post('periode/activate', 'PeriodeController@setAktifPeriode');
    Route::resource('fakultas', 'FakultasController');
    Route::resource('prodi', 'ProdiController');
    
    Route::resource('admin_fakultas', 'AdminFakultasController');
    Route::resource('mahasiswa', 'MahasiswaController');

    Route::resource('prestasi', 'PrestasiController');
    Route::resource('pendidikan', 'RiwayatPendidikanController');
    Route::resource('karya', 'KaryaController');
    Route::resource('penelitian', 'PenelitianController');
    Route::resource('organisasi', 'OrganisasiController');
    Route::resource('pekerjaan', 'PengalamanPekerjaanController');
    Route::resource('sertifikat', 'SertifikasiController');
    
});

Route::group(array('prefix' => 'mahasiswa', 'as' => 'mahasiswa.', 'namespace' => 'Mahasiswa','middleware'=>'auth:mahasiswa'), function(){
	Route::get('/', 'HomeController@getIndex');
	Route::get('index', 'HomeController@getIndex')->name('index');
    Route::get('print/{lang}', 'HomeController@getPrint')->name('print');
    Route::get('profil', 'HomeController@getProfil');
    Route::post('profil', 'HomeController@postProfil');

    Route::resource('karya', 'KaryaController');
    Route::resource('organisasi', 'OrganisasiController');
    Route::resource('pekerjaan', 'PengalamanKerjaController');
    Route::resource('prestasi', 'PrestasiController');
    Route::resource('penelitian', 'PenelitianController');
    Route::resource('pendidikan', 'RiwayatPendidikanController');
    Route::resource('sertifikat', 'SertifikasiController');
});

Route::get('/images/{filename}', function ($filename)
{
    $path = storage_path() . '/app/public/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/images/mahasiswa/{filename}', function ($filename)
{
    $path = storage_path() . '/app/mahasiswa/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::group(array('prefix' => 'images', 'as' => 'images'), function(){

    Route::get('pendidikan/{filename}', 'ImageDownloadController@getImageRiwayatPendidikan');

    Route::get('prestasi/kegiatan/{filename}', 'ImageDownloadController@getImageKegiatanPrestasi');
    Route::get('prestasi/sertifikat/{filename}', 'ImageDownloadController@getImageSertifikatPrestasi');

    Route::get('sertifikat/{filename}', 'ImageDownloadController@getImageSertifikat');

    Route::get('karya/{filename}', 'ImageDownloadController@getImageKarya');

    Route::get('penelitian/{filename}', 'ImageDownloadController@getImagePenelitian');

    Route::get('organisasi/{filename}', 'ImageDownloadController@getImageOrganisasi');

    Route::get('pekerjaan/{filename}', 'ImageDownloadController@getImagePekerjaan');

});
